# Improved Youtube Search - Back
## Configuration
### Google Account
+ You have to create a Google account in order to use this application (https://accounts.google.com/signup/v2/webcreateaccount)
+ Activate the YouTube Data API v3 in the API Console (https://console.developers.google.com)
+ Create an API key and store it's value in the environnment variable YOUTUBE_API_KEY. You will use the API key to make API requests that do not require user authorization. For example, you do not need user authorization to retrieve information about a public YouTube channel.
+ Create an OAuth 2.0 client ID and set the application type to Other. Store the CLIENT ID in the environnment variable OAUTH_CLIENT_ID and the CLIENT SECRET in OAUTH_CLIENT_SECRET. You need to use OAuth 2.0 credentials for requests that require user authorization. For example, you need user authorization to retrieve information about the currently authenticated user's YouTube channel.
* For more information, consult the Google quickstart (https://developers.google.com/youtube/v3/quickstart/java)

### Azure Account
+ You have to get your Azure Account's connection string for the Azure Queue and store it in the environnment variable AZURE_KEY.

### Other environnement variables
+ SERVER_URL is the url to connect to the server
+ SERVER_ROUTE_POSTSTATE is the endpoint to send the progress of a search
+ SERVER_ROUTE_POSTRESULT is the endpoint to send the results of a search
+ SERVER_ROUTE_SIGNIN is the endpoint to log in the server
+ SERVER_LOGIN is the login used to connect to the server
+ SERVER_PWD is the password associated to the login
+ HEROKU_API_KEY is used to deploy the application on your Heroku's account

### Maven
This application is a Maven application, you have to install it (https://maven.apache.org/download.cgi)