package esgi.pa.webscraping.units;

import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.shared.Utils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtilsTest {

    @Test
    public void should_get_a_search_from_string() {
        String s = "{\"urlVideo\":\"https://www.youtube.com/watch?v=c1x_mYX46CI\",\"categoryVideo\":\"Gaming\",\"youtuberName\":\"RandomBlackGamer\",\"keyWords\":\"SpiderManPS5, SpiderManPS4\",\"username\":\"usertest\",\"idSearch\":131,\"idUser\":1}";
        Search search = new Search("https://www.youtube.com/watch?v=c1x_mYX46CI",
                                                "Gaming",
                                                "RandomBlackGamer",
                                                "SpiderManPS5, SpiderManPS4",
                                                "usertest",
                                                131,
                                                1);
        assertEquals(search, Utils.stringToSearch(s));
    }

    @Test
    public void should_get_score_from_two_texts() {
        assertEquals(5.0, (double)Utils.getScoreFormTwoTexts("bonjour test", "test bonsoir", 5));
    }

    @Test
    public void should_get_score_from_two_texts_in_different_order() {
        assertEquals(10.0, (double)Utils.getScoreFormTwoTexts("bonjour test", "test bonjour", 5));
    }

    @Test
    public void should_get_score_from_two_different_texts() {
        assertEquals(0.0, (double)Utils.getScoreFormTwoTexts("bonjour test", "bonsoir bonsoir", 5));
    }

    @Test
    public void should_get_score_from_two_similar_texts_with_case() {
        assertEquals(10.0, (double)Utils.getScoreFormTwoTexts("bonjour test", "BONJOUR TEST", 5));
    }

    @Test
    public void should_get_score_from_two_texts_empty() {
        assertEquals(0.0, (double)Utils.getScoreFormTwoTexts("", "", 5));
    }

    @Test
    public void should_get_score_from_two_texts_null() {
        assertEquals(0.0, (double)Utils.getScoreFormTwoTexts("bonjour test", null, 5));
    }

    @Test void should_get_int_from_state() {
        assertEquals(10, (int)Utils.stateToInt(Utils.States.STEP_10));
    }
}
