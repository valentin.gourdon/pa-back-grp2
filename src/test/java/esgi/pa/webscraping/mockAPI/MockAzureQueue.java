package esgi.pa.webscraping.mockAPI;

import java.util.ArrayList;

public class MockAzureQueue {
    private final String SEND_QUEUE = "gr2-tasks-queue-send-usertest";
    private final String RECEIVE_QUEUE = "gr2-tasks-queue-receive-usertest";

    private ArrayList<String> azureSendQueue;
    private ArrayList<String> azureReceiveQueue;

    public MockAzureQueue() {
        this.azureReceiveQueue = new ArrayList<>();
        this.azureSendQueue = new ArrayList<>();
    }

    public void postAzureMessageReceive(String sendMessage, String queueName) {
        if(queueName.equals(RECEIVE_QUEUE))
            this.azureReceiveQueue.add(sendMessage);
        else
            this.azureSendQueue.add(sendMessage);
    }

    public String getAzureMessage(String queueName) {
        String message;
        if(queueName.equals(SEND_QUEUE)) {
            message = this.azureSendQueue.get(0);
            this.azureSendQueue.remove(0);
        } else {
            message = this.azureReceiveQueue.get(0);
            this.azureReceiveQueue.remove(0);
        }
        return message;
    }

    public long getQueueSize(String queueName) {
        if(queueName.equals(SEND_QUEUE))
            return this.azureSendQueue.size();
        else
            return this.azureReceiveQueue.size();
    }
}
