package esgi.pa.webscraping.mockAPI;

import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.Video;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MockYoutubeSearch {

    public MockYoutubeSearch() {}

    public Video getInfosForYTVideo(String youtubeUrl, Search search, Video video, boolean fullInfos) throws MalformedURLException {
        if(fullInfos) {
            return new Video(new URL(youtubeUrl),
                    "Gaming",
                    "Dahmien7",
                    "escapefromtarkov, eft",
                    "usertest",
                    new URL("https://i.ytimg.com/vi/M8oaSEn5xug/maxresdefault.jpg"),
                    search);
        } else {
            video.setCategories("Gaming");
            video.setTags("escapefromtarkov, eft");
            return video;
        }
    }

    public ArrayList<Video> searchVideos(String searchParam, Search search) throws MalformedURLException {
        ArrayList<Video> returnArray = new ArrayList<>();
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=W0Auks3LgEo"), "LA COURSE POURSUITE DES ENFERS...", "dahmien7", new URL("https://i.ytimg.com/vi/W0Auks3LgEo/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=_K8Q_dzZ8sk"), "NEW RECORD PERSO SOLO - dahmien7", "dahmien7", new URL("https://i.ytimg.com/vi/_K8Q_dzZ8sk/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=Oru2Sebkm-s"), "JE DEVIENS RICHE EN TUANT CRESUS !", "dahmien7", new URL("https://i.ytimg.com/vi/Oru2Sebkm-s/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=G3ymkcMZVUw"), "BOIRE OU S\'ENFUIR IL FAUT CHOISIR...", "dahmien7", new URL("https://i.ytimg.com/vi/G3ymkcMZVUw/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=hcza4iLCdUc"), "NOUVELLE ARME : LE MOSIN !!", "dahmien7", new URL("https://i.ytimg.com/vi/hcza4iLCdUc/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=kBg2IrLhzMg"), "NEW MAP PUBG VIKENDI - dahmien7", "dahmien7", new URL("https://i.ytimg.com/vi/kBg2IrLhzMg/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=qjqcacsyeks"), "LES RAISONS DE MON BAN...", "dahmien7", new URL("https://i.ytimg.com/vi/qjqcacsyeks/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=RtnOLmdFBPc"), "HIGHLIGHTS #100 - dahmien7", "dahmien7", new URL("https://i.ytimg.com/vi/RtnOLmdFBPc/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=m6Y_a3aHhgI"), "MON RETOUR DANS L\'ESPORT ??", "dahmien7", new URL("https://i.ytimg.com/vi/m6Y_a3aHhgI/default.jpg"), search));
        returnArray.add(new Video(new URL("https://www.youtube.com/watch?v=u3WK1jvFJYo"), "PLAN Q SUR LAB !!", "dahmien7", new URL("https://i.ytimg.com/vi/u3WK1jvFJYo/default.jpg"), search));

        return returnArray;
    }
}
