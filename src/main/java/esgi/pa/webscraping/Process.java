package esgi.pa.webscraping;

import esgi.pa.webscraping.azure.AzureQueue;
import esgi.pa.webscraping.models.Result;
import esgi.pa.webscraping.models.Search;
import esgi.pa.webscraping.models.StateSearch;
import esgi.pa.webscraping.models.Video;
import esgi.pa.webscraping.server.Communication;
import esgi.pa.webscraping.shared.Utils;
import esgi.pa.webscraping.youtube.YoutubeSearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Process {

    ArrayList<Search> searchQueue;              // List with all the video to search and their parameters (author, categories ...)
    ArrayList<Video> videoQueue;                // List with all the video to search and their information
    ArrayList<String> azureReceiveQueuesList;   // List of all the Receive Azure Queues
    ArrayList<Video> resultVideoList;           // List of all the results for each video to search (associated to the username queue)
    ArrayList<Result> returnResultList;         // List of all the result to send to the queue

    private static final Integer MAX_VIDEO_TO_RETURN = 10;

    public Process(ArrayList<String> azureReceiveQueuesList,
                   ArrayList<Search> searchQueue,
                   ArrayList<Video> videoQueue,
                   ArrayList<Video> resultVideoList,
                   ArrayList<Result> returnResultList) {

        this.azureReceiveQueuesList = azureReceiveQueuesList;
        this.searchQueue = searchQueue;
        this.videoQueue = videoQueue;
        this.resultVideoList = resultVideoList;
        this.returnResultList = returnResultList;

    }

    /**
     * Read from Azure Queue and write in VideoQueue
     * */
    public synchronized void getAzureMessage() throws IOException {
        // For all the queues :
        while(true) {
            // Get all the receive queues name. True = sendQueues on Azure
            azureReceiveQueuesList = AzureQueue.getAllQueues();

            // For each queue
            for (String queue : azureReceiveQueuesList) {
                Search search;
                // While the queue is not empty
                while ((search = AzureQueue.getAzureMessage(queue))!= null) {
                    // Set state for the current search
                    search.setStateSearch(new StateSearch(search.getIdSearch(), search.getIdUser(), Utils.States.STEP_10));

                    // If the first send to the server is wrong don't add the video : there is no use to go further.
                    if(Communication.sendToServer(search.getStateSearch())) {

                        // Add it to the List
                        searchQueue.add(search);

                        // Get the Youtube information about this video
                        Video video = YoutubeSearch.getInfosForYTVideo(search.getUrlVideo(), search, null, true);

                        // Set state for the current search
                        search.updateStateSearch(Utils.States.STEP_20);
                        Communication.sendToServer(search.getStateSearch());

                        // If the information are found, add it to the list
                        if (video != null) {
                            // Set the title for the Search based on the infos retrieved before.
                            video.getSearch().setTitle(video.getTitle());
                            videoQueue.add(video);
                        }
                        notifyAll();
                    } else {
                        System.out.println("Unable to reach server. Search for " + search.getUrlVideo() + " aborted.");
                    }
                }
                try {
                    this.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Read from Video Queue and write in Result Queue
     * */
    public synchronized void searchVideo() throws IOException {
        while(true) {
            if (this.videoQueue.isEmpty()) {
                try {
                    wait();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // For all new videos
            for(Iterator<Video> iterator = videoQueue.iterator(); iterator.hasNext(); ) {
                resultVideoList = new ArrayList<>();
                ArrayList<Video> tmpChannel;
                ArrayList<Video> tmpCategories;
                ArrayList<Video> tmpTags;

                Video video = iterator.next();

                // Set state for the current search
                video.getSearch().updateStateSearch(Utils.States.STEP_30);
                Communication.sendToServer(video.getSearch().getStateSearch());

                // Search based on the channel.
                // If no channel specified by the User, we add the video's channel to the Search
                if (video.getSearch().getYoutuberName().isEmpty())
                    video.getSearch().setYoutuberName(video.getChannel());
                tmpChannel = YoutubeSearch.searchVideos(video.getSearch().getYoutuberName(), video.getSearch());
                assert tmpChannel != null;
                resultVideoList.addAll(tmpChannel);

                // Set state for the current search
                video.getSearch().updateStateSearch(Utils.States.STEP_50);
                Communication.sendToServer(video.getSearch().getStateSearch());

                // Search based on the category.
                // If no categories specified by the User, we add the video's categories to the Search
                if (video.getSearch().getCategoryVideo().isEmpty())
                    video.getSearch().setCategoryVideo(video.getCategories());
                tmpCategories = YoutubeSearch.searchVideos(video.getSearch().getCategoryVideo(), video.getSearch());
                assert tmpCategories != null;
                Utils.addAllIfDifferent(resultVideoList, tmpCategories);

                // Set state for the current search
                video.getSearch().updateStateSearch(Utils.States.STEP_70);
                Communication.sendToServer(video.getSearch().getStateSearch());

                // Search based on the keywords.
                // If no keywords specified by the User, we add the video's keywords to the Search
                if (video.getSearch().getKeyWords().isEmpty())
                    video.getSearch().setKeyWords(video.getTags());
                tmpTags = YoutubeSearch.searchVideos(video.getSearch().getKeyWords(), video.getSearch());
                assert tmpTags != null;
                Utils.addAllIfDifferent(resultVideoList, tmpTags);

                // Set state for the current search
                video.getSearch().updateStateSearch(Utils.States.STEP_80);
                Communication.sendToServer(video.getSearch().getStateSearch());

                //Sort the resultVideoList by score (check Video's compareTo())
                Collections.sort(resultVideoList);

                // Limit the number of video returned
                while (resultVideoList.size() > MAX_VIDEO_TO_RETURN) {
                    resultVideoList.remove(resultVideoList.size() - 1);
                }

                // Create the Result and add it to the list
                Result result = new Result(video.getSearch(), resultVideoList);
                returnResultList.add(result);

                iterator.remove();
                notifyAll();
                try {
                    this.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Read from Result Queue and send to the server
     */
    public synchronized void sendAzureMessage() throws IOException {
        while(true) {
            if (this.returnResultList.isEmpty()) {
                try {
                    wait();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // For each Result
            for(Iterator<Result> iterator = returnResultList.iterator(); iterator.hasNext();) {
                Result result = iterator.next();

                // Set state for the current search
                result.getSearch().updateStateSearch(Utils.States.STEP_90);
                Communication.sendToServer(result.getSearch().getStateSearch());

                // Send the results to the server
                Communication.sendToServer(result);

                // Set state for the current search
                result.getSearch().updateStateSearch(Utils.States.STEP_100);
                Communication.sendToServer(result.getSearch().getStateSearch());

                iterator.remove();
            }
        }
    }
}
