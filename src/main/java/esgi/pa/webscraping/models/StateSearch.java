package esgi.pa.webscraping.models;

import esgi.pa.webscraping.shared.Utils;

public class StateSearch {
    private int idSearch;

    private int idUser;

    private Utils.States state;

    public StateSearch(int idSearch, int idUser, Utils.States state) {
        this.idSearch = idSearch;
        this.idUser = idUser;
        this.state = state;
    }

    public Utils.States getState() {
        return state;
    }

    public void setState(Utils.States state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "StateSearch{" +
                "idSearch=" + idSearch +
                ", idUser=" + idUser +
                ", state='" + state + '\'' +
                '}';
    }

    public String toJSON() {
        return "{\"idSearch\": " + idSearch +
                ", \"idUser\": " + idUser +
                ", \"state\": \"" + Utils.stateToInt(state) +
                "\"}";
    }

}
