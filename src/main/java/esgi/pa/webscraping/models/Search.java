package esgi.pa.webscraping.models;

import esgi.pa.webscraping.shared.Utils;

import java.io.Serializable;
import java.util.Objects;

public class Search implements Serializable {

    private Integer idSearch;

    private String urlVideo;

    private String categoryVideo;

    private String youtuberName;

    private String keyWords;

    private String username;

    private String title;

    private Integer idUser;

    private StateSearch stateSearch;

    // Used for the creation from a JSON
    public Search() {
    }

    public Search(String urlVideo, String categoryVideo, String youtuberName, String keyWords, String username, Integer idSearch, Integer idUser) {
        this.idSearch = idSearch;
        this.urlVideo = urlVideo;
        this.categoryVideo = categoryVideo;
        this.youtuberName = youtuberName;
        this.keyWords = keyWords;
        this.username = username;
        this.title = null;
        this.idUser = idUser;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public String getYoutuberName() {
        return youtuberName;
    }

    public void setYoutuberName(String youtuberName) {
        this.youtuberName = youtuberName;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getUsername() {
        return username;
    }

    public Integer getIdSearch() {
        return idSearch;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public StateSearch getStateSearch() {
        return stateSearch;
    }

    public void setStateSearch(StateSearch stateSearch) {
        this.stateSearch = stateSearch;
    }

    public void updateStateSearch(Utils.States state) {
        this.stateSearch.setState(state);
    }

    @Override
    public String toString() {
        return "Search :" +
                "\n\tid = " + idSearch +
                "\n\turlVideo = " + urlVideo +
                "\n\ttitleVideo = " + title +
                "\n\tcategoryVideo = " + categoryVideo +
                "\n\tyoutuberName = " + youtuberName +
                "\n\tkeyWords = " + keyWords +
                "\n\tusername = " + username +
                "\n\tidUser = " + idUser;
    }

    public String toJSON() {
        return "\"idSearch\":" + idSearch +
                ",\"urlVideo\":\"" + urlVideo + "\"" +
                ",\"categoryVideo\":\"" + categoryVideo + "\"" +
                ",\"youtuberName\":\"" + youtuberName + "\"" +
                ",\"keyWords\":\"" + keyWords + "\"" +
                ",\"username\":\"" + username + "\"" +
                ",\"idUser\":" + idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Search)) return false;
        Search search = (Search) o;
        return Objects.equals(idSearch, search.idSearch) &&
                Objects.equals(urlVideo, search.urlVideo) &&
                Objects.equals(categoryVideo, search.categoryVideo) &&
                Objects.equals(youtuberName, search.youtuberName) &&
                Objects.equals(keyWords, search.keyWords) &&
                Objects.equals(username, search.username) &&
                Objects.equals(title, search.title);
    }
}
