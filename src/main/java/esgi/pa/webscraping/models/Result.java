package esgi.pa.webscraping.models;

import java.util.ArrayList;

public class Result {
    private Search search;
    private ArrayList<Video> listVideo;

    public Result(Search search, ArrayList<Video> listVideo) {
        this.search = search;
        this.listVideo = listVideo;
    }

    public Search getSearch() {
        return search;
    }

    public String arrayVideoToJSON() {
        StringBuilder ret = new StringBuilder("[");
        for (int i = 0; i < this.listVideo.size(); i++) {
            if( i != listVideo.size()-1) {
                ret.append("{").append(listVideo.get(i).toJSON()).append("}, ");
            }
            else {
                ret.append("{").append(listVideo.get(i).toJSON()).append("}");
            }
        }
        ret.append("]");
        return ret.toString();
    }

    public String toJSON() {
        return "{\"urlsearch\":{" + this.search.toJSON() + "}, " +
                    "\"urlfind\":" + arrayVideoToJSON() + "}";
    }

    public String toString() {
        StringBuilder ret = new StringBuilder("Result : \n" + this.search.toString());
        for(Video video : this.listVideo) {
            ret.append("\n").append(video.toString());
        }
        return ret.toString();
    }
}
