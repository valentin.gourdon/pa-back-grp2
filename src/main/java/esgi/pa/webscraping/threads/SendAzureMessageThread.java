package esgi.pa.webscraping.threads;

import esgi.pa.webscraping.Process;

import java.io.IOException;

public class SendAzureMessageThread extends Thread {

    Process process;

    public SendAzureMessageThread(Process process) {
        this.process = process;
    }

    public void run() {
        try {
            this.process.sendAzureMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
