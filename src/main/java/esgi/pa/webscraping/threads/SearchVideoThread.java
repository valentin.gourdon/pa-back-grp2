package esgi.pa.webscraping.threads;

import esgi.pa.webscraping.Process;

import java.io.IOException;

public class SearchVideoThread extends Thread {

    Process process;

    public SearchVideoThread(Process process) {
        this.process = process;
    }

    public void run() {
        try {
            this.process.searchVideo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
